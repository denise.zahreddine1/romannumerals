package org.example;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
         //assertTrue(result.equals("I"));
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test11();
        test123();
        test1003();
        test2003();
        test3250();
        test510();
        test901();

    }
    public void test1() {
        String result = App.convertToChiffreRomain(1);
        assertEquals("I", result);
    }
    public void test2() {
        String result = App.convertToChiffreRomain(2);
        assertEquals("II", result);
    }
    public void test3() {
        String result = App.convertToChiffreRomain(3);
        assertEquals("III", result);
    }
    public void test4() {
        String result = App.convertToChiffreRomain(4);
        assertEquals("IV", result);
    }

    public void test5() {
        String result = App.convertToChiffreRomain(5);
        assertEquals("V", result);
    }
    public void test6() {
        String result = App.convertToChiffreRomain(6);
        assertEquals("VI", result);
    }

    public void test11() {
        String result = App.convertToChiffreRomain(11);
        assertEquals("XI", result);
    }
    public void test123() {
        String result = App.convertToChiffreRomain(123);
        assertEquals("CXXIII", result);
    }
    public void test1003() {
        String result = App.convertToChiffreRomain(1003);
        System.out.println(result);
        assertEquals("MIII", result);
    }

    public void test3250() {
        String result = App.convertToChiffreRomain(3250);
        assertEquals("MMMCCL", result);
    }
    public void test2003() {
        String result = App.convertToChiffreRomain(3250);
        assertEquals("MMMCCL", result);
    }
    public void test510() {
        String result = App.convertToChiffreRomain(510);
        assertEquals("DX", result);
    }
    public void test901() {
        String result = App.convertToChiffreRomain(901);
        assertEquals("CMI", result);
    }
    public void test904() {
        String result = App.convertToChiffreRomain(904);
        assertEquals("CMIV", result);
    }
    public void test309() {
        String result = App.convertToChiffreRomain(309);
        assertEquals("CCCIX", result);
    }


}
