package org.example;


/**
 * Hello world!
 */
public class App {
    public static String convertToChiffreRomain(int number) {


        String romainChiffre = "";
        int reste = number;

        while (reste != 0) {
            if (reste - 1000 >= 0) {
                romainChiffre = romainChiffre + "M";
                reste = reste - 1000;
                continue;
            }
            if (reste - 900 >= 0) {
                romainChiffre = romainChiffre + "CM";
                reste = reste - 900;
                continue;
            }
            if (reste - 500 >= 0) {
                romainChiffre = romainChiffre + "D";
                reste = reste - 500;
                continue;
            }
            if (reste - 400 >= 0) {
                romainChiffre = romainChiffre + "CD";
                reste = reste - 400;
                continue;
            }
            if (reste - 100 >= 0) {
                romainChiffre = romainChiffre + "C";
                reste = reste - 100;
                continue;
            }
            if (reste - 90 >= 0) {
                romainChiffre = romainChiffre + "XC";
                reste = reste - 90;
                continue;
            }
            if(reste-50>=0){
                romainChiffre = romainChiffre + "L";
                reste = reste - 50;
                continue;
            }

            if(reste-40>=0){
                romainChiffre = romainChiffre + "XL";
                reste = reste - 40;
                continue;
            }
            if (reste - 10 >= 0) {
                romainChiffre = romainChiffre + "X";
                reste = reste - 10;
                continue;
            }
            if (reste - 9 >= 0) {
                romainChiffre = romainChiffre + "IX";
                reste = reste - 9;
                continue;
            }
            if (reste - 5 >= 0) {
                romainChiffre = romainChiffre + "V";
                reste = reste - 5;
                continue;
            }
            if(reste-4>=0){
                romainChiffre = romainChiffre + "IV";
                reste = reste - 4;
                continue;
            }
            if (reste < 4) {
                romainChiffre = romainChiffre + "I";
                reste = reste - 1;
                continue;
            }
        }
        return romainChiffre;
    }

    public static void main(String[] args) {
        convertToChiffreRomain(709);
        System.out.println( convertToChiffreRomain(709));
    }
}
